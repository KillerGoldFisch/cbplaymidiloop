package net.gliewe.cb.playmidiloop;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

public class Main extends JavaPlugin {

    private static Main _instance = null;

    public static Main getInstance() {
        return _instance;
    }

    private File _midiFile = null;

    private BukkitScheduler _scheduler = null;

    private PlayerPlayerManager _playerPlayerManager;

    public File getMidiFile() {
        return this._midiFile;
    }

    public PlayerPlayerManager getPlayerPlayerManager() { return this._playerPlayerManager; }


    @Override
    public void onEnable(){
        this.saveDefaultConfig();

        _instance = this;

        if(!this.getConfig().getBoolean("active", true))
            return;

        String midiFileName = this.getConfig().getString("midiFile");

        if(midiFileName == null) {
            this.getLogger().warning("Node Midi file defined");
            return;
        }

        this._midiFile = new File(midiFileName);

        if(!this._midiFile.canRead()) {
            this.getLogger().warning("Midi file not readable");
            return;
        }

        this._playerPlayerManager = new PlayerPlayerManager(this);
        this._playerPlayerManager.setStopped(!this.getConfig().getBoolean("midiplaying", true));

        this._scheduler = Bukkit.getServer().getScheduler();
        this._scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                Main.this._playerPlayerManager.Update();
            }
        }, 20, 20);
    }

    @Override
    public void onDisable() {
        _instance = null;

        if(this._scheduler != null) {
            this._scheduler.cancelAllTasks();
        }

        this._scheduler = null;

        if(this._playerPlayerManager != null)
            this._playerPlayerManager.Stop();

        this._playerPlayerManager = null;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("midiaddblacklist")){
            if(args.length != 1)
                return false;
            Player player = this.getServer().getPlayer(args[0]);
            if(player == null) {
                sender.sendMessage("Player not found");
                return true;
            }

            List<String> blacklist = this.getConfig().getStringList("playerBlacklist");

            blacklist.add(player.getName());
            this._playerPlayerManager.getPlayerPlayer(player).StopPlaying();

            this.getConfig().set("playerBlacklist", blacklist);
            this.saveConfig();

            return true;
        } else if(cmd.getName().equalsIgnoreCase("midiremblacklist")) {
            if(args.length != 1)
                return false;
            Player player = this.getServer().getPlayer(args[0]);
            if(player == null) {
                sender.sendMessage("Player not found");
                return true;
            }

            List<String> blacklist = this.getConfig().getStringList("playerBlacklist");

            blacklist.remove(player.getName());

            this.getConfig().set("playerBlacklist", blacklist);
            this.saveConfig();

            return true;
        } else if(cmd.getName().equalsIgnoreCase("midisetplaying")) {
            if(args.length != 1)
                return false;

            boolean playing = Boolean.parseBoolean(args[0]);

            this.getPlayerPlayerManager().setStopped(!playing);

            this.getConfig().set("midiplaying", playing);
            this.saveConfig();

            return true;
        }
        return false;
    }
}