package net.gliewe.cb.playmidiloop;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

/**
 * User: kevin
 * Date: 06.12.13
 * Time: 16:39
 */
public class PlayerPlayerManager implements Listener {
    private Main _plugin = null;
    private List<PlayerPlayer> _players = null;
    private boolean _stopped = false;

    public Plugin get_plugin() { return this._plugin; }

    public PlayerPlayerManager(Main plugin) {
        this._plugin = plugin;
        this._players = new ArrayList<PlayerPlayer>();

        for(Player p : Bukkit.getServer().getOnlinePlayers()) {
            this._players.add(new PlayerPlayer(this._plugin, p));
        }

        this._plugin.getServer().getPluginManager().registerEvents(this, this._plugin);
    }

    public void Update() {

        for(PlayerPlayer pp : this._players)
            try {
                if(this._stopped)
                    pp.StopPlaying();
                else
                    pp.Update();
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        this._players.add(
                new PlayerPlayer(
                        this._plugin,
                        event.getPlayer()
                )
        );
    }

    @EventHandler
    public void  onPlayerQuit(PlayerQuitEvent e) {
        this.RemovePlayer(e.getPlayer());
    }

    public void RemovePlayer(Player player) {
        ArrayList<PlayerPlayer> toRemove = new ArrayList<PlayerPlayer>();

        for(PlayerPlayer pp : this._players) {
            if(pp.get_player() == player)
                toRemove.add(pp);
        }

        for(PlayerPlayer pp : toRemove) {
            this._players.remove(toRemove);
            pp.StopPlaying();
        }
    }

    public PlayerPlayer getPlayerPlayer(Player p) {
        for(PlayerPlayer pp : this._players)
            if(pp.get_player() == p)
                return pp;
        return null;
    }

    public void Stop() {
        for(PlayerPlayer pp : this._players)
            pp.StopPlaying();
    }

    public boolean getStopped() { return this._stopped; }
    public void setStopped(boolean stopped) { this._stopped = stopped; }
}
