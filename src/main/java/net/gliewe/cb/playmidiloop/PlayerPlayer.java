package net.gliewe.cb.playmidiloop;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequencer;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * User: kevin
 * Date: 06.12.13
 * Time: 15:38
 */
public class PlayerPlayer {
    private Player _player = null;

    private Sequencer _sequencer = null;

    private Main _plugin = null;

    private HashSet<Player> _playerSet;

    public Sequencer get_sequencer() { return this._sequencer; }

    public Main get_plugin() { return this._plugin; }

    public Player get_player(){ return this._player; }

    public PlayerPlayer(Main plugin, Player player) {
        this._player = player;
        this._plugin = plugin;

        this._playerSet = new HashSet<Player>();
        this._playerSet.add(this._player);
    }

    public boolean isBlackListed() {
        for(String name : this._plugin.getConfig().getStringList("playerBlacklist")) {
            if(name.equalsIgnoreCase(this._player.getName()))
                return true;
        }
        return false;
    }

    public void Update() throws MidiUnavailableException, InvalidMidiDataException, IOException {
        if(this.isBlackListed()) {
            this.StopPlaying();
        } else {
            this.StartPlaying();
        }
    }

    public void StartPlaying() throws MidiUnavailableException, InvalidMidiDataException, IOException {
        if(this._sequencer != null) {
            if(this._sequencer.isRunning())
                return;
        }

        this._sequencer = net.gliewe.cb.killerlib.util.midi.MidiUtil.playerMidi(
                this._plugin.getMidiFile(),
                (float)0.9,
                this._playerSet);
        this._sequencer.start();
    }

    public void StopPlaying() {
        if(this._sequencer != null) {
            this._sequencer.stop();
            try {
                this._sequencer.getTransmitter().setReceiver(null);
            } catch (MidiUnavailableException e) {
                e.printStackTrace();
            }
            this._sequencer = null;
        }
    }
}
